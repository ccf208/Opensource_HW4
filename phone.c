#include <stdio.h>
#include <string.h>

#include "phone.h"

char password[] = "qwer1234";

void Register();
void Allprint();
void Personalprint();

int userNo = 0;
int rmBuffer=0;

int main()
{
    int service;

    do
    {
        printf("\n***Phone number Management***\n");
        printf("\n1.Register    2.Search All    3.Search Name    4.Delete    5.Exit\n");
        printf("\nSelect Menu: ");
        scanf("%d", &service); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);

        if(service <= 5 && service >= 1)
        {
            switch (service)
            {
                case 1: Register(); break;
                case 2: Allprint(); break;
                case 3: Personalprint(); break;
                case 4: Delete(); break;
            }
        }
        else
        {
            printf("\nIncorrect Insert.\n");
        }

    }while (service != 5);

    printf("\nStop program.\n");

    return 0;
}
