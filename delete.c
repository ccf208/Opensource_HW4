#include <stdio.h>
#include <string.h>

#include "phone.h"

#define MAX 50

extern int rmBuffer;
extern int userNo;

void Delete()
{
    struct phone *pbook;
    pbook = &book;
    int numtoDelete = 0;
    char enterName[10];

    printf("\n4.Delete\n");
    printf("Please Enter Name to Delete: ");
    scanf("%9s", enterName); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);

    for(int i = 0; i < userNo; i++){
        if (strcmp((pbook+i)->UserName, enterName) == 0) {
            printf("%s Delete Complete!", enterName);
            break;
        } numtoDelete++;
    }

    for(int i = numtoDelete; i < userNo; i++){
      strcpy((pbook+i)->UserName,((pbook+i)+1)->UserName);

      strcpy((pbook+i)->UserNumber,((pbook+i)+1)->UserNumber);
    }

    userNo--;
}
